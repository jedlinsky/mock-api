const fs = require('fs')
const path = require('path')
const express = require('express')
const cors = require('cors')

const app = express()

const apis = ['authors', 'comments', 'posts']

app.use(cors())

app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, 'index.html'))
})

apis.forEach(api => {
  app.get(`/${api}`, (req, res) => {
    res.setHeader('Content-Type', 'application/json')

    fs.readFile(path.join(__dirname, `${api}.json`), 'utf-8', (err, data) => {
      data = JSON.parse(data)

      res.json(data)
    })
  })
})

app.listen(5000, () => console.log('Mock is listening on port 5000!'))
