# Mock API

## Popis úkolu
Vytvoření aplikace pomocí React 16, která načte data ze vzdáleného serveru (https://mock-api-ioqlhynosn.now.sh) a bude s nimi pracovat po dobu jejího běhu.

## Zadání
* Na hlavní stránce se bude nacházet výpis všech článků (jejich název + datum vytvoření).
* Po kliknutí na článek se uživatel přesměruje (nastane i změna URI) na adresu, která se nachází v property "slug". Tato "stránka" se načte i v případě, že uživatel udělá refresh.
* Na detailu článku uživatel uvidí jeho název, datum vytvoření, autora (celé jméno a avatar; pokud je autor žena, zobrazí se jeho jméno červeně, pokud muž, modře), obsah článku (pozor, může obsahovat i paragrafy) a příslušné komentáře.
* Komentáře budou obsahovat datum vytvoření, jméno, e-mail a obsah (rovněž může obsahovat paragrafy).
* Po kliknutí na autora dojde k přesměrování na jeho stránku, kde bude zobrazeno jeho celé jméno, pohlaví, e-mail, avatar, výpis zkušeností a jeho biografie. Rovněž lze udělat refresh stránky.
* Budou fungovat prohlížečová tlačítka pro přechod na předchozí/následující stránku.

## Endpointy
**URL:** https://mock-api-ioqlhynosn.now.sh

**POZOR!** Občas může při delším nepoužití trvat, než se server rozjede, protože spí.

### /authors
    Array<{
      id: number,
      first_name: string,
      last_name: string,
      gender: 'Female' | 'Male',
      email: string,
      avatar: string,
      skills: string[],
      bio: string
    }>

### /comments
    Array<{
      id: number,
      content: string,
      post_id: number,
      name: string,
      email?: string,
      created_at: Date
    }>

### /posts
    Array<{
      id: number,
      title: string,
      content: string,
      author_id: number,
      created_at: Date,
      slug: string
    }>
